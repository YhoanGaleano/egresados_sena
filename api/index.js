'use strict'

//Importación de librerias y archivos exportados
var mongoose = require('mongoose');
var app = require("./app");

//Configuración del puerto por el cual correra nuestra API
var port = process.env.PORT || 3977;

//Conexión a mongo
mongoose.connect('mongodb://localhost:27017/egresados_SENA', (err, res) => {
    if (err) {
        throw err;
    } else {
        console.log("La base de datos de egresados SENA esta corriendo correctamente");
    }

    //Metodo para correr el servidor
    app.listen(port, function() {
        console.log("Servidor del API RESTful de egresados SENA escuchando en http://localhost:" + port);
    })
});