'use strict'

var express = require('express');
var ProgramaController = require('../controllers/programa');
var mdAuth = require('../middlewares/authenticated');

var api = express.Router();

api.get("/programa/:id", /*mdAuth.ensureAuth,*/ ProgramaController.getPrograma);
api.get("/programas/:nivelFormacion?/:areaFormacion?", /*mdAuth.ensureAuth,*/ ProgramaController.getProgramas);

api.post("/programa", /*mdAuth.ensureAuth,*/ ProgramaController.savePrograma);
api.put("/programa/:id", /*mdAuth.ensureAuth,*/ ProgramaController.updatePrograma);
api.put("/programa/:id", /*mdAuth.ensureAuth,*/ ProgramaController.changeStatusPrograma);

module.exports = api;