'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DepartamentoSchema = Schema({
    nombreDepartamento: String
});

module.exports = mongoose.model('Departamento', DepartamentoSchema);