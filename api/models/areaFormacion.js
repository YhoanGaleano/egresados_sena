'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AreaFormacionSchema = Schema({
    nombreAreaFormacion: String,
    centro: {
        type: Schema.ObjectId,
        ref: 'Centro'
    }

});

module.exports = mongoose.model('AreaFormacion', AreaFormacionSchema);