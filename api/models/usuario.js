'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UsuarioSchema = Schema({

    usuario: String,
    contrasena: String,

    /*
        roles son: super admin, administradores de centro y instructores
    */
    rol: {
        type: Schema.ObjectId,
        ref: "Rol",
    },
    centro: {
        type: Schema.ObjectId,
        ref: 'Centro'
    },

    /*
        Info para los aprendices
    */

    tipoDocumento: {
        type: Schema.ObjectId,
        ref: 'TipoDocumento'
    },
    numeroDocumento: String,

    nombreCompleto: String,
    correoElectronico: String,
    lugarResidencia: String,
    telefonoPrimario: String,
    celular: String,

    avatar: String

});

module.exports = mongoose.model('Usuario', UsuarioSchema);