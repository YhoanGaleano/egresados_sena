'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AprendizSchema = Schema({

    usuario: String,
    contrasena: String,

    /*
        Info para los aprendices
    */

    tipoDocumento: {
        type: Schema.ObjectId,
        ref: 'TipoDocumento'
    },
    numeroDocumento: String,

    nombreCompleto: String,
    correoElectronico: String,
    lugarResidencia: String,
    telefonoPrimario: String,
    telefonoSecundario: String,
    celular: String,

    avatar: String

});

module.exports = mongoose.model('Aprendiz', AprendizSchema);