'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FichaSchema = Schema({
    numeroFicha: String,
    convenioMediaTecnica: Boolean,
    entidadCertificadora: String,
    registroAcademico: String,
    programa: {
        type: Schema.ObjectId,
        ref: 'Programa'
    },
    fechaInicio: String,
    fechaFinalizacion: String
});

module.exports = mongoose.model('Ficha', FichaSchema);