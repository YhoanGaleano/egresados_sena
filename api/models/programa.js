'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProgramaSchema = Schema({
    nombrePrograma: String,
    nivelFormacion: {
        type: Schema.ObjectId,
        ref: 'NivelFormacion'
    },
    areaFormacion: {
        type: Schema.ObjectId,
        ref: 'AreaFormacion'
    },
    estado: Boolean
});

module.exports = mongoose.model('Programa', ProgramaSchema);