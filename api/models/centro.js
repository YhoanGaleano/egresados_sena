'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Centro = Schema({
    nombreCentro: String,
    ciudad: {
        type: Schema.ObjectId,
        ref: 'Ciudad'
    },
    imagen: String
});

module.exports = mongoose.model('Centro', CentroSchema);