'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CiudadSchema = Schema({
    nombreCiudad: String,
    departamento: {
        type: Schema.ObjectId,
        ref: 'Departamento'
    }
});

module.exports = mongoose.model('Ciudad', CiudadSchema);