'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DetalleFichaSchema = Schema({
    fechaCertificacion: String,
    estudioCertificado: Boolean,
    ficha: {
        type: Schema.ObjectId,
        ref: 'Ficha'
    },
    aprendiz: {
        type: Schema.ObjectId,
        ref: 'Aprendiz'
    },
    estadoCertificado: {
        type: Schema.ObjectId,
        ref: 'EstadoCertificado'
    }
});

module.exports = mongoose.model('DetalleFicha', DetalleFichaSchema);