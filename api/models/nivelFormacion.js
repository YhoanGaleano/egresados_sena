'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NivelFormacion = Schema({
    nombreNivelFormacion: String,
    estado: Boolean
});

module.exports = mongoose.model('NivelFormacion', NivelFormacionSchema);