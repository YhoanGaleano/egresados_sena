'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TipoDocumentoSchema = Schema({
    nombreTipoDocumento: String,
});

module.exports = mongoose.model('TipoDocumento', TipoDocumentoSchema);