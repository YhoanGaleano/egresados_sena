'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RolSchema = Schema({
    nombreRol: String
});

module.exports = mongoose.model('Rol', RolSchema);