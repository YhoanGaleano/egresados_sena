'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EstadoCertificadoSchema = Schema({
    nombreEstado: String
});

module.exports = mongoose.model('EstadoCertificado', EstadoCertificadoSchema);