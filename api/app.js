'use strict'

//Cargamos express que es la libreria para las rutas o api
var express = require('express');

//Parsear lo que venga de la petición HTTP
var bodyParser = require('body-parser');

//instanciamos el objeto
var app = express();

//Cargar rutas
var programa_routes = require('./routes/programa');

//Configurar body-parser
/*app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());*/

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

//Configurar las cabeceras http
app.use((req, res, next) => {
    // Accesos a todos los dominios
    res.header("Access-Control-Allow-Origin", "*");

    res.header("Access-Control-Allow-Headers", "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method");

    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");

    res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");

    next();
});

//Rutas base
app.use("/api", programa_routes);

app.get("/", function(req, res) {
    res.status(200).send({ message: "Bienvenido al API de Egresados SENA" });
})

//Exportamos el modulo de app
module.exports = app;