'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = "beOZUa=cGky4xGH";

exports.createToken = function(usuario) {

    var payload = {

        //id del documento de la base de datos
        sub: usuario._id,
        name: usuario.nombreCompleto,
        user: usuario.usuario,
        email: usuario.correoElectronico,
        role: usuario.rol,
        image: usuario.avatar,
        iat: moment().unix(),
        exp: moment().add(30, 'days').unix
    };

    return jwt.encode(payload, secret);

};