'use strict'

var Nivel = require('../models/nivelFormacion');

function getNivel(req, res) {
    var nivelId = req.params.id;

    Nivel.findById(nivelId, (error, nivel) => {

        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar buscar un nivel",
                object: null,
                response: false
            });
        } else {
            if (!nivel) {

                res.status(404).send({
                    message: "No existe el nivel que estas buscando",
                    object: null,
                    response: false
                });

            } else {

                res.status(200).send({
                    message: "Nivel encontrado correctamente",
                    object: nivel,
                    response: true
                });

            }
        }

    });
}

function getNiveles(req, res) {

    Nivel.find().sort('nombreNivelFormacion').exec((error, niveles) => {

        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar listar los niveles de formación",
                object: null,
                response: false
            });
        } else {
            if (!niveles) {

                res.status(404).send({
                    message: "No existen niveles de formación",
                    object: null,
                    response: false
                });

            } else {

                return res.status(200).send({
                    message: "Niveles encontrados correctamente",
                    object: niveles,
                    response: true
                });

            }
        }

    });
}

function updateNivel(req, res) {
    var nivelId = req.params.id;
    var update = req.body;

    Nivel.findByIdAndUpdate(nivelId, update, (err, nivelUpdated) => {
        if (err) {

            res.status(500).send({
                message: 'Error  en el servidor'
            });

        } else {
            if (!areaStored) {
                res.status(404).send({
                    message: 'no se actualizo '
                });
            }
        } else {
            res.status(200).send({ nivel: nivelUpdated });
        }
    })

}

function saveNivel(req, res) {
    var nivel = new nivelFormacion();

    var params = req.body;
    nivel.nombreNivelFormacion = params.nombreNivelFormacion;


    nivel.save((err, nivelStored) => {
        if (err) {

            res.status(500).send({ message: 'Error  en el servidor' });

        } else {
            if (!nivelStored) {
                res.status(404).send({ message: 'no se actualizo ' });
            }
        } else {
            res.status(200).send({ nivel: nivelStored });
        }
    });

}

function changeStatusPrograma(req, res) {
    var programaId = req.params.id;
    var estado = req.body.estado;

    Programa.findOneAndUpdate({ "_id": programaId }, { "$set": { "estado": estado } }, (error, programaUpdated) => {
        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar actualizar un programa",
                object: null,
                response: false
            });
        } else {
            if (!programaUpdated) {

                res.status(404).send({
                    message: "No se ha podido actualizar el programa o no existe.",
                    object: null,
                    response: false
                });

            } else {

                res.status(200).send({
                    message: "Programa actualizado correctamente",
                    object: programaUpdated,
                    response: true
                });

            }
        }
    });
}

module.export = {
    getNivel,
    saveNivel,
    getNiveles,
    updateNivel,
    deleteNivel

}