'use strict'

//Para trabajar con el sistema de ficheros
//No se instala, ya node lo trae
var fs = require('fs');
var path = require('path');
var mongoosePaginate = require('mongoose-pagination');

var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getArtist(req, res){

	var artistId = req.params.id;

	Artist.findById(artistId, (error, artist) => {

		if (error) {
			res.status(500).send({
				message: "Ocurrio un error en el servidor al intentar buscar",
				object: null,
				response: false
			});
		}else{
			if (!artist) {

				res.status(404).send({
					message: "No existe el artista que estas buscando",
					object: null,
					response: false
				});

			}else{

				res.status(200).send({
					message: "Artista encontrado correctamente",
					object: artist,
					response: true 
				});

			}
		}

	});
}

function saveArtist(req, res) {

	var artist = new Artist();

	var params = req.body;
	artist.name = params.name;
	artist.description = params.description;
	artist.image = null;
	
	artist.save((error, artistStored) => {

		if (error) {
			res.status(500).send({
				message: "Ocurrio un error en el servidor al intentar guardar",
				object: null,
				response: false
			});
		}else{
			if (!artistStored) {

				res.status(404).send({
					message: "Ocurrio un error al intentar guardar el artista",
					object: null,
					response: false
				});

			}else{

				res.status(200).send({
					message: "Artista guardado correctamente",
					object: artistStored,
					response: true 
				});

			}
		}

	})
}

function getArtists(req, res){
	var page = req.params.page == null ? 1 : req.params.page;
	console.log(page);
	var itemsPerPage = 12;

	Artist.find().sort('name').paginate(page, itemsPerPage, function(error, artists, total){

		if (error) {
			res.status(500).send({
				message: "Ocurrio un error en el servidor al intentar listar",
				object: null,
				response: false
			});
		}else{
			if (!artists) {

				res.status(404).send({
					message: "No existen artistas",
					object: null,
					response: false
				});

			}else{

				return res.status(200).send({
					message: "Artista encontrado correctamente",
					object: {
						totalItems: total,
						lists: artists
					},
					response: true 
				});

			}
		}

	});
}

function updateArtist(req, res) {
	var artistId = req.params.id;
	var update = req.body;

	Artist.findByIdAndUpdate(artistId, update, (error, artistUpdated) => {
		if (error) {
			res.status(500).send({
				message: "Ocurrio un error en el servidor al intentar actualizar",
				object: null,
				response: false
			});
		}else{
			if (!artistUpdated) {

				res.status(404).send({
					message: "No se ha podido actualizar el artista o no existe.",
					object: null,
					response: false
				});

			}else{

				res.status(200).send({
					message: "Artista actualizado correctamente",
					object: artistUpdated,
					response: true 
				});

			}
		}
	});
}

function deleteArtist(req, res) {
	var artistId = req.params.id;

	Artist.findByIdAndRemove(artistId, (error, artistRemoved) => {

		if (error) {
			res.status(500).send({
				message: "Ocurrio un error en el servidor al intentar eliminar",
				object: null,
				response: false
			});
		}else{
			if (!artistRemoved) {

				res.status(404).send({
					message: "No se ha podido eliminar el artista o no existe.",
					object: null,
					response: false
				});

			}else{

				var pathToUnlink = './uploads/artists/'+artistRemoved.image;

				fs.exists(pathToUnlink, function(exists) {
					if(exists) {
						fs.unlink(pathToUnlink);
					}
				});

				Album.find({artist: artistRemoved._id}).remove((error, albumRemoved) => {

					if (error) {
						res.status(500).send({
							message: "Ocurrio un error en el servidor al intentar eliminar el album",
							object: null,
							response: false
						});
					}else{
						if (!albumRemoved) {

							res.status(404).send({
								message: "No se ha podido eliminar los albums del artista o no existen.",
								object: null,
								response: false
							});

						}else{

							
							Song.find({album: albumRemoved._id}).remove((error, songRemoved) => {

								if (error) {
									res.status(500).send({
										message: "Ocurrio un error en el servidor al intentar eliminar las canciones",
										object: null,
										response: false
									});
								}else{
									if (!songRemoved) {

										res.status(404).send({
											message: "No se ha podido eliminar las canciones del artista o no existen.",
											object: null,
											response: false
										});

									}else{

										res.status(200).send({
											message: "Artista eliminado correctamente",
											object: artistRemoved,
											response: true 
										});

									}
								}

							});
						}
					}

				});

			}
		}

	});
}

function uploadImagen(req, res) {
	var artistId = req.params.id;
	var file_name = "No subido...";

	if (req.files) {

		var file_path = req.files.image.path;
		var file_split = file_path.split('/');
		var file_name = file_split[2];
		var ext_split = file_name.split('\.');
		var file_ext = ext_split[1];

		if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'gif') {

			Artist.findByIdAndUpdate(artistId, {image: file_name}, (error, artistUpdated) => {

				if (error) {
					res.status(500).send({
						message: "Ocurrio un error en el servidor al intentar actualizar la imagen del artista",
						object: null,
						response: false
					});
				}else{
					if (!artistUpdated) {

						res.status(404).send({
							message: "No se ha podido actualizar la imagen del artista o no existe.",
							object: null,
							response: false
						});

					}else{

						var pathToUnlink = './uploads/artists/'+artistUpdated.image;

						fs.exists(pathToUnlink, function(exists) {
							if(exists) {
								fs.unlink(pathToUnlink);
							}
						});

						res.status(200).send({
							message: "Imagen del artista actualizada correctamente",
							object: {
								artistUpdated: artistUpdated,
								image: file_name
							},
							response: true 
						});

					}
				}

			});

		}else{
			res.status(500).send({
				message: "La extensión no es correcta",
				object: null,
				response: false 
			});
		}

	}else{
		res.status(404).send({
			message: "No has subido ninguna imagen...",
			object: null,
			response: false 
		});
	}
}

function getImageFile(req, res) {
	var imageFile = req.params.imageFile;

	var pathImage = './uploads/artists/'+imageFile;
	
	fs.exists(pathImage, function(exists) {
		if(exists) {

			res.sendFile(path.resolve(pathImage));
			
		}else{
			res.status(404).send({
				message: "No se encuentra la imagen del artista o no existe.",
				object: null,
				response: false
			});
		}
	});
}

module.exports = {
	getArtist,
	saveArtist,
	getArtists,
	updateArtist,
	deleteArtist,
	uploadImagen,
	getImageFile
};