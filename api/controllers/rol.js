'use strict'
var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var Rol = require('../Modelos_DB/4_rol');
var jwt = require('../services/jwt');

function saveRol(req, res){
  var rol = new Rol();

  var params = req.body;

  console.log(params);

  rol.nombreRol = params.nombreRol;

  if (rol.nombreRol != null ) {
    rol.save((err, rolStored)=> {
      if (err) {
        res.status(500).send({message: 'Error al guardar el rol'})

      }else {
        if (!rolStored) {
          res.status(404).send({message: 'No se ha registrado el rol'})
        }else {
          res.status(200).send({Rol: rolStored})
        }
      }
    });

  }

function updateRol(req, res){
  var rolId = req.params.id;
  var update = req.body;

  Rol.findByIdAndUpdate(rolId, update, (err, rolUpdated) => {
    if (err) {
      res.status(500).send({message: 'Error al actualizar el rol'});
    }else {
      if(!rolUpdated){
        res.status(404).send({message: 'No se ha podido actualizar el rol'});

      }else {
        res.status(200).send({rol: rolUpdated});
      }
    }
  });
}

function deleteRol(req, res){
  var rolId = req.params.id;
  Rol.findByIdAndRemove(rolId, (err, rolRemoved) =>{
    if (err) {
      res.status(500).send({message: 'Error en el servidor'});
    }else {
      if (!rolRemoved) {
        res.status(404).send({message: 'No se ha borrado el rol'});
      }else {
        res.status(200).send({rol: rolRemoved});
      }
    }
  });
}

function getRol(req, res){
  var rolId = req.params.id;

  Rol.findById(rolId, (err, rol) => {
    if (err) {
      res.status(500).send({message: 'Error en la peticion. '});

    }else {
      if (!rol) {
        res.status(404).send({message: 'el rol no existe'});
      }else {
        res.status(200).send({rol});
      }
    }
  });
}

module.exports = {
  saveRol,
  updateRol,
  deleteRol,
  getRol
};
