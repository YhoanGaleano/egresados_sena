'use strict'
var path = require('path');
var fs = require('fs');
var mongoosePaginate = require('mongoose-Pagination');
var Departamento = require('../Modelos_DB/1_departamento');
var Ciudad = require('../Modelos_DB/2_ciudad');
var Centro = require('../Modelos_DB/3_centro');


function saveCentro(req, res){
  var centro = new Centro();

  var params = req.body;
  centro.nombreCentro = params.nombreCentro;
  centro.ciudad = params.ciudad;

  centro.save((err, centroStored) => {
    if (err) {
      res.status(500).send({message: 'Error en el servidor'});
    }else {
      if (!centroStored) {
        res.status(404).send({message: 'Error en el servidor'});
      }else {
        res.status(200).send({centro: centroStored});
      }
    }
  });
}


function updateCentro(req, res){
  var centroId = req.params.id;
  var update = req.body;

  Centro.findByIdAndUpdate(centroId, update, (err, centroUpdated) =>{
    if (err) {
      res.status(500).send({message: 'Error en el servidor'});
    }else {
      if (!centroUpdated) {
        res.status(404).send({message: 'No se ha actualizado centro'});
      }else {
        res.status(200).send({centro: centroUpdated});
      }
    }
  });
}

function deleteCentro(req, res){
  var centroId = req.params.id;
  Centro.findByIdAndRemove(centroId, (err, centroRemoved) =>{
    if (err) {
      res.status(500).send({message: 'Error en el servidor'});
    }else {
      if (!centroRemoved) {
        res.status(404).send({message: 'No se ha borrado el centro'});
      }else {
        res.status(200).send({centro: centroRemoved});
      }
    }
  });

}



function getCentro(req, res){
var centroId = req.params.id;

Centro.findById(centroId).populate({path: 'ciudad'}).exec((err, song) => {
  if (err) {
    res.status(500).send({message: 'Error en la peticion'});
  }else {
    if (!centro) {
      res.status(404).send({message: 'No existe el centro'});
    }else {
      res.status(200).send({centro});
    }
  }
});

}


function getcentros(req, res){
  var ciudadId = req.params.ciudad;

  if (!ciudadId) {
    var find = Centro.find({}).sort('number');

  }else {
    var find = Song.find({ciudad: ciudadId}).sort('number');
  }
  find.populate({
    path: 'ciudad',
    populate: {
      path: 'departamento',
      model: 'Departamento'
    }
  }).exec(function(err, centros){
    if (err) {
      res.status(500).send({message: 'Error en la peticion'});
    }else {
      if (!centros) {
        res.status(404).send({message: 'No existe el centro'});
      }else {
        res.status(200).send({centros});
      }
    }
  });
}

module.exports = {
saveCentro,
updateCentro,
deleteCentro,
getCentro,
getcentros
};
