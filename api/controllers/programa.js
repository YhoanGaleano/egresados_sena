'use strict'

var Programa = require('../models/programa');

function getPrograma(req, res) {

    var programaId = req.params.id;

    Programa.findById(programaId, (error, programa) => {

        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar buscar un programa",
                object: null,
                response: false
            });
        } else {
            if (!programa) {

                res.status(404).send({
                    message: "No existe el programa que estas buscando",
                    object: null,
                    response: false
                });

            } else {

                res.status(200).send({
                    message: "Programa encontrado correctamente",
                    object: programa,
                    response: true
                });

            }
        }

    });
}

function getProgramas(req, res) {

    var nivelFormacionId = req.params.nivelFormacion;
    var areaFormacionId = req.params.areaFormacion;

    if (nivelFormacionId != "" && areaFormacionId != "") {

        var find = Programa.find({ nivelFormacion: nivelFormacionId, areaFormacion: areaFormacionId }).sort('nombrePrograma');

    } else if (nivelFormacionId != "") {

        var find = Programa.find({ nivelFormacion: nivelFormacionId }).sort('nombrePrograma');

    } else if (areaFormacionId != "") {

        var find = Programa.find({ areaFormacion: areaFormacionId }).sort('nombrePrograma');

    } else {

        var find = Programa.find({}).sort('nombrePrograma');

    }

    find.populate({
        path: 'nivelFormacion'
    }).populate({
        path: 'areaFormacion'
    }).exec((error, programas) => {

        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar listar los programas",
                object: null,
                response: false
            });
        } else {
            if (!programas) {

                res.status(404).send({
                    message: "No existen programas",
                    object: null,
                    response: false
                });

            } else {

                return res.status(200).send({
                    message: "Programas encontrados correctamente",
                    object: programas,
                    response: true
                });

            }
        }

    });
}

function savePrograma(req, res) {

    var programa = new Programa();

    var params = req.body;
    programa.nombrePrograma = params.nombrePrograma;
    programa.nivelFormacion = params.nivelFormacion;
    programa.areaFormacion = params.areaFormacion;
    programa.estado = true;

    programa.save((error, programaStored) => {

        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar guardar un programa",
                object: null,
                response: false
            });
        } else {
            if (!programaStored) {

                res.status(404).send({
                    message: "Ocurrio un error al intentar guardar un programa",
                    object: null,
                    response: false
                });

            } else {

                res.status(200).send({
                    message: "Programa guardado correctamente",
                    object: programaStored,
                    response: true
                });

            }
        }

    })
}

function updatePrograma(req, res) {
    var programaId = req.params.id;
    var update = req.body;

    Programa.findByIdAndUpdate(programaId, update, (error, programaUpdated) => {
        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar actualizar un programa",
                object: null,
                response: false
            });
        } else {
            if (!programaUpdated) {

                res.status(404).send({
                    message: "No se ha podido actualizar el programa o no existe.",
                    object: null,
                    response: false
                });

            } else {

                res.status(200).send({
                    message: "Programa actualizado correctamente",
                    object: programaUpdated,
                    response: true
                });

            }
        }
    });
}

function changeStatusPrograma(req, res) {
    var programaId = req.params.id;
    var estado = req.body.estado;

    Programa.findOneAndUpdate({ "_id": programaId }, { "$set": { "estado": estado } }, (error, programaUpdated) => {
        if (error) {
            res.status(500).send({
                message: "Ocurrio un error en el servidor al intentar actualizar un programa",
                object: null,
                response: false
            });
        } else {
            if (!programaUpdated) {

                res.status(404).send({
                    message: "No se ha podido actualizar el programa o no existe.",
                    object: null,
                    response: false
                });

            } else {

                res.status(200).send({
                    message: "Programa actualizado correctamente",
                    object: programaUpdated,
                    response: true
                });

            }
        }
    });
}

module.exports = {
    getPrograma,
    getProgramas,
    savePrograma,
    updatePrograma,
    changeStatusPrograma
};